//
//  GMLoadingView.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation
import UIKit

private let kLoadingViewTag = 47365

class GMLoadingView: UIView {
    // MARK: - Creation functions.
    class func gmLoadingView(backgrounColor: UIColor, alpha: CGFloat) -> GMLoadingView {
        let loadingView = GMLoadingView.gmViewFromNib() as! GMLoadingView
        loadingView.backgroundColor = backgrounColor
        loadingView.alpha = alpha
        loadingView.tag = kLoadingViewTag
        
        return loadingView
    }
    
    // MARK: - Getter functions.
    class func gmLoadingViewTag() -> Int {
        return kLoadingViewTag
    }
}
