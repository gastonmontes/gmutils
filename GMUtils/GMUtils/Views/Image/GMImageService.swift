//
//  GMImageService.swift
//  GMUtils
//
//  Created by Gaston Montes on 15/07/2020.
//  Copyright © 2020 Gaston  Montes. All rights reserved.
//

import UIKit

typealias GMImageServiceSuccess = (UIImage) -> Void

class GMImageService {
    // MARK: - Class vars.
    private static let imageServiceCache = NSCache<NSString, UIImage>()
    
    // MARK: - Vars.
    private var currentDataTask: URLSessionDataTask?
    
    // MARK: - Fetch images functions.
    func gmImageFromURL(_ urlString: String, sucess: GMImageServiceSuccess?, fail: GMServiceFailHandler?) {
        if let imageFromCache = GMImageService.imageServiceCache.object(forKey: urlString as NSString) {
            sucess?(imageFromCache)
            return
        }
        
        guard let imageURL = URL(string: urlString) else {
            fail?(GMBaseServiceError(domain: "image", description: "Could not load image URL", code: -376482))
            return
        }
        
        URLSession.shared.dataTask(with: imageURL) { data, response, error in
            guard let imageData = data else {
                fail?(GMBaseServiceError(domain: "image", description: "Could not retriev image data", code: -373279))
                return
            }
            
            if let imageToCache = UIImage(data: imageData) {
                GMImageService.imageServiceCache.setObject(imageToCache, forKey: urlString as NSString)
                sucess?(imageToCache)
            } else {
                fail?(GMBaseServiceError(domain: "image", description: "Could not retriev image data", code: -373279))
            }
        }.resume()
    }
    
    // MARK: - Cancel images functions.
    func gmImageCancelCurrent() {
        if let currentTask = self.currentDataTask {
            currentTask.cancel()
        }
    }
}
