//
//  GMLabel+AttributedText.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func gmLabelSet(text: String, lineSpacing: Int) {
        let attributedString = NSMutableAttributedString(string: text)
        let paragraphStyle = NSMutableParagraphStyle()
        
        paragraphStyle.alignment = NSTextAlignment.center
        paragraphStyle.lineSpacing = CGFloat(lineSpacing)
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle,
                                      value:paragraphStyle,
                                      range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
}
