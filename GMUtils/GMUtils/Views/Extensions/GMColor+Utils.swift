//
//  GMColor+Utils.swift
//  GMUtils
//
//  Created by Gaston Montes on 17/04/2020.
//  Copyright © 2020 Gaston  Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
}
