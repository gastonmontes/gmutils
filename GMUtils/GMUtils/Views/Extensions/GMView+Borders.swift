//
//  GMView+Borders.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import UIKit

private let kBorderBottomViewTag = 39453

extension UIView {
    // MARK: - Add Border functions.
    func gmBorderTop(borderSize: Float, borderColor: UIColor)  {
        let bottomBorderView = UIView(frame: .zero)
        bottomBorderView.backgroundColor = borderColor
        self.addSubview(bottomBorderView)
        
        bottomBorderView.gmConstraintsSize(height: borderSize)
        bottomBorderView.gmConstraintsMarginToSuperView(leftMargin: 0)
        bottomBorderView.gmConstraintsMarginToSuperView(rightMargin: 0)
        bottomBorderView.gmConstraintsMarginToSuperView(topMargin: 0)
    }
    
    func gmBorderBottom(borderSize: Float, borderColor: UIColor)  {
        let bottomBorderView = UIView(frame: .zero)
        bottomBorderView.backgroundColor = borderColor
        bottomBorderView.tag = kBorderBottomViewTag
        self.addSubview(bottomBorderView)
        
        bottomBorderView.gmConstraintsSize(height: borderSize)
        bottomBorderView.gmConstraintsMarginToSuperView(leftMargin: 0)
        bottomBorderView.gmConstraintsMarginToSuperView(rightMargin: 0)
        bottomBorderView.gmConstraintsMarginToSuperView(bottomMargin: 0)
    }
    
    func gmBorderLeft(borderSize: Float, borderColor: UIColor)  {
        let bottomBorderView = UIView(frame: .zero)
        bottomBorderView.backgroundColor = borderColor
        self.addSubview(bottomBorderView)
        
        bottomBorderView.gmConstraintsSize(width: borderSize)
        bottomBorderView.gmConstraintsMarginToSuperView(leftMargin: 0)
        bottomBorderView.gmConstraintsMarginToSuperView(topMargin: 0)
        bottomBorderView.gmConstraintsMarginToSuperView(bottomMargin: 0)
    }
    
    func gmBorderRight(borderSize: Float, borderColor: UIColor)  {
        let bottomBorderView = UIView(frame: .zero)
        bottomBorderView.backgroundColor = borderColor
        self.addSubview(bottomBorderView)
        
        bottomBorderView.gmConstraintsSize(width: borderSize)
        bottomBorderView.gmConstraintsMarginToSuperView(rightMargin: 0)
        bottomBorderView.gmConstraintsMarginToSuperView(topMargin: 0)
        bottomBorderView.gmConstraintsMarginToSuperView(bottomMargin: 0)
    }
    
    func gmBorder(withRadius cornerRadius: Float, borderWidth: Float, borderColor: UIColor) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.layer.borderWidth = CGFloat(borderWidth)
        self.layer.borderColor = borderColor.cgColor
    }
    
    func gmBorderShadow(withColor color: UIColor, opacity: Float, offset: CGSize, radius: Float) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = CGFloat(radius)
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    func gmBorder(withRadius cornerRadius: Float) {
        self.gmBorder(withRadius: cornerRadius, borderWidth: 0.0, borderColor: UIColor.clear)
    }
    
    // MARK: - Remover border functions.
    func gmBorderRemoveBottom() {
        if let bottomView = self.viewWithTag(kBorderBottomViewTag) {
            bottomView.removeFromSuperview()
        }
    }
}
