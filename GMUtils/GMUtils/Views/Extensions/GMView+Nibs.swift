//
//  GMView+Nibs.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIView {    
    class func gmViewFromNib() -> UIView? {
        let loadedNibs = Bundle.main.loadNibNamed(String(describing: self), owner: self, options: nil)
        
        guard let view = loadedNibs?.first as? UIView else {
            return nil;
        }
        
        return view;
    }
}
