//
//  GMApplication+URL.swift
//  GMUtils
//
//  Created by Gaston Montes on 17/04/2020.
//  Copyright © 2020 Gaston  Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    func gmApplicationOpen(urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }
        
        UIApplication.shared.open(url, options: [ : ], completionHandler: nil)
    }
    
    func gmApplicationOpen(url: URL) {
        UIApplication.shared.open(url, options: [ : ], completionHandler: nil)
    }
}
