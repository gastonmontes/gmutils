//
//  GMView+Frame.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    // MARK: - Seiz functions.
    func gmFrameHeight() -> CGFloat {
        return self.frame.size.height
    }
    
    func gmFrameWidth() -> CGFloat {
        return self.frame.size.width
    }
    
    func gmFrameSet(height: CGFloat) {
        let newFrame = CGRect(x: self.gmFrameX(), y: self.gmFrameY(), width: self.gmFrameWidth(), height: height)
        self.frame = newFrame
    }
    
    func gmFrameSet(width: CGFloat) {
        let newFrame = CGRect(x: self.gmFrameX(), y: self.gmFrameY(), width: width, height: self.gmFrameHeight())
        self.frame = newFrame
    }
    
    // MARK: - Origin functions.
    func gmFrameX() -> CGFloat {
        return self.frame.origin.x
    }
    
    func gmFrameY() -> CGFloat {
        return self.frame.origin.y
    }
    
    func gmFrameSet(x: CGFloat) {
        let newFrame = CGRect(x: x, y: self.gmFrameY(), width: self.gmFrameWidth(), height: self.gmFrameHeight())
        self.frame = newFrame
    }
    
    func gmFrameSet(y: CGFloat) {
        let newFrame = CGRect(x: self.gmFrameX(), y: y, width: self.gmFrameWidth(), height: self.gmFrameHeight())
        self.frame = newFrame
    }
    
    // MARK: - Combined functions.
    func gmFrameMaxX() -> CGFloat{
        return self.gmFrameX() + self.gmFrameWidth()
    }
    
    func gmFrameMaxY() -> CGFloat {
        return self.gmFrameY() + self.gmFrameHeight()
    }
}
