//
//  GMView+Constraints.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    // MARK: - Size constraints.
    func gmConstraintsSizeSameAsSuperview() {
        guard let superview = self.superview else {
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.topAnchor.constraint(equalTo: superview.topAnchor, constant: 0).isActive = true
        self.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: 0).isActive = true
        self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: 0).isActive = true
        self.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: 0).isActive = true
    }
    
    func gmConstraintsSize(height: Float) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.heightAnchor.constraint(equalToConstant: CGFloat(height)).isActive = true
    }
    
    func gmConstraintsSize(width: Float) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.widthAnchor.constraint(equalToConstant: CGFloat(width)).isActive = true
    }
    
    // MARK: - Margin constraints.
    func gmConstraintsMarginToSuperView(topMargin: Float, leftMargin: Float, bottomMargin: Float, rightMargin: Float) {
        guard let superview = self.superview else {
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.topAnchor.constraint(equalTo: superview.topAnchor, constant: CGFloat(topMargin)).isActive = true
        self.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: CGFloat(leftMargin)).isActive = true
        self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: CGFloat(bottomMargin)).isActive = true
        self.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: CGFloat(rightMargin)).isActive = true
    }
    
    func gmConstraintsMarginToSuperView(topMargin: Float) {
        guard let superview = self.superview else {
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.topAnchor.constraint(equalTo: superview.topAnchor, constant: CGFloat(topMargin)).isActive = true
    }
    
    func gmConstraintsMarginToSuperView(bottomMargin: Float) {
        guard let superview = self.superview else {
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: CGFloat(bottomMargin)).isActive = true
    }
    
    func gmConstraintsMarginToSuperView(leftMargin: Float) {
        guard let superview = self.superview else {
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: CGFloat(leftMargin)).isActive = true
    }
    
    func gmConstraintsMarginToSuperView(rightMargin: Float) {
        guard let superview = self.superview else {
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: CGFloat(rightMargin)).isActive = true
    }
    
    // MARK: - Center functions.
    func gmConstraintsCenterXInSuperview()  {
        guard let superview = self.superview else {
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.centerXAnchor.constraint(equalTo: superview.centerXAnchor, constant: 0).isActive = true
    }
    
    func gmConstraintsCenterYInSuperview()  {
        guard let superview = self.superview else {
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.centerYAnchor.constraint(equalTo: superview.centerYAnchor, constant: 0).isActive = true
    }
}
