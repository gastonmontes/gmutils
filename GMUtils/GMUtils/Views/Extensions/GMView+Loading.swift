//
//  GMView+Loading.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func gmLoadingShow() {
        if self.loadingView() == nil {
            let loadingView = GMLoadingView.gmLoadingView(backgrounColor: UIColor.black, alpha: 0.5);
            self.addSubview(loadingView)
            loadingView.gmConstraintsSizeSameAsSuperview()
        }
    }
    
    func gmLoadingStop() {
        if let loadingView = self.loadingView() {
            loadingView.removeFromSuperview()
        }
    }
    
    private func loadingView() -> GMLoadingView? {
        let loadingView = self.viewWithTag(GMLoadingView.gmLoadingViewTag()) as? GMLoadingView
        return loadingView
    }
}
