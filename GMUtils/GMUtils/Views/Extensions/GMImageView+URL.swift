//
//  GMImageView+URL.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func gmImageViewSet(imageFromURLString urlString: String, placeholder: UIImage?) {
        self.image = placeholder
        
        let imageCache = NSCache<NSString, UIImage>()
        
        if let imageFromCache = imageCache.object(forKey: urlString as NSString) {
            self.image = imageFromCache
            return
        }
        
        guard let imageURL = URL(string: urlString) else {
            return
        }
        
        URLSession.shared.dataTask(with: imageURL) { [unowned self] data, response, error in
            guard let imageData = data else {
                return
            }
            
            if let imageToCache = UIImage(data: imageData) {
                DispatchQueue.main.async {
                    print("caching image: %@", urlString)
                    imageCache.setObject(imageToCache, forKey: urlString as NSString)
                    self.image = imageToCache
                }
            }
            }.resume()
    }
}
