//
//  GMNetworkReachability.swift
//  Gastón Montes
//
//  Created by Gastón Montes on 09/06/2022.
//

import SystemConfiguration
import CoreTelephony
import Foundation
import Network

enum GMNetworkConnectionType {
    case unknown
    case noConnection
    case cellular_unknown
    case cellular_2G
    case cellular_3G
    case cellular_4G
    case cellular_5G
    case wifi
    case wired
    case loopback
}

class GMNetworkReachability: ObservableObject {
    // MARK: - Vars.
    private let networkMonitor = NWPathMonitor()
    private let networkQueue = DispatchQueue.global(qos: .background)
    
    @Published private(set) var networkIsReachable: Bool = false
    @Published private(set) var networkConnectionType: GMNetworkConnectionType = .unknown
    
    // MARK: - Initialization.
    init() {
        self.checkConnection()
    }
    
    deinit {
        self.networkMonitor.cancel()
    }
    
    // MARK: - Network reachability functions.
    func checkConnection() {
        self.networkMonitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                self.networkIsReachable = true
                self.networkConnectionType = self.networkConnetionType(networkPath: path)
            } else {
                self.networkIsReachable = false
                self.networkConnectionType = .noConnection
            }
        }
        
        self.networkMonitor.start(queue: self.networkQueue)
    }
    
    // MARK: - Network connection type functions.
    private func networkConnetionType(networkPath: NWPath) -> GMNetworkConnectionType {
        if networkPath.usesInterfaceType(.wifi) {
            return .wifi
        } else if networkPath.usesInterfaceType(.cellular) {
            return self.networkCellularConnectionType()
        } else if networkPath.usesInterfaceType(.wiredEthernet) {
            return .wired
        } else if networkPath.usesInterfaceType(.loopback) {
            return .loopback
        }
        
        return .unknown
    }
    
    private func networkCellularConnectionType() -> GMNetworkConnectionType {
        let networkInfo = CTTelephonyNetworkInfo()
        let carrierType = networkInfo.serviceCurrentRadioAccessTechnology
        
        guard let carrierTypeName = carrierType?.first?.value else {
            return .cellular_unknown
        }
        
        switch carrierTypeName {
        case CTRadioAccessTechnologyGPRS, CTRadioAccessTechnologyEdge, CTRadioAccessTechnologyCDMA1x:
            return .cellular_2G
        case CTRadioAccessTechnologyLTE:
            return .cellular_4G
        case CTRadioAccessTechnologyNRNSA, CTRadioAccessTechnologyNR:
            return .cellular_5G
        default:
            return .cellular_3G
        }
    }
}
