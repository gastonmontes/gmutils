//
//  GMString+URL.swift
//  GMUtils
//
//  Created by Gaston Montes on 17/04/2020.
//  Copyright © 2020 Gaston  Montes. All rights reserved.
//

import Foundation

extension String {
    func gmStringCreateURLQueryPercentScape() -> String {
        // does not include "?" or "/" due to RFC 3986.
        let generalDelimitersToEncode = ":#[]@"
        let subDelimitersToEncode = "!$&'()*+,;="
        var allowedCharacterSet = CharacterSet.urlHostAllowed
        allowedCharacterSet.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) ?? ""
    }
}
