//
//  GMDate+String.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation

extension Date {
    func gmDateString(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        
        return formatter.string(from: self)
    }
}
