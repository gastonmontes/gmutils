//
//  GMDictionary+Operations.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation

extension Dictionary {
    mutating func gmDictionaryAddElements(from: Dictionary?) {
        if let newDict = from, newDict.count > 0 {
            newDict.forEach({ (key, value) in self[key] = value })
        }
    }
}
