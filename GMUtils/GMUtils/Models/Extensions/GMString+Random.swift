//
//  GMString+Random.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation

extension String {
    static func gmStringRandom(lenght: Int) -> String? {
        var dataString = Data(count: lenght)
        let result = dataString.withUnsafeMutableBytes({
            SecRandomCopyBytes(kSecRandomDefault, lenght, $0.baseAddress!)
        })
        
        if result == errSecSuccess {
            return dataString.base64EncodedString()
        } else {
            return nil
        }
    }
}
