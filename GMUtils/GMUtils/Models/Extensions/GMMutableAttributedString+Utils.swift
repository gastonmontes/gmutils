//
//  GMMutableAttributedString+Utils.swift
//  GMUtils
//
//  Created by Gaston Montes on 17/04/2020.
//  Copyright © 2020 Gaston  Montes. All rights reserved.
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    // MARK: - Font functions.
    func gmAttributedStringSet(mainFont: UIFont) {
        let allWordRange = (self.string as NSString).range(of: self.string)
        self.addAttribute(NSAttributedString.Key.font, value: mainFont, range: allWordRange)
    }
    
    func gmAttributedStringSet(font: UIFont, forSubstring: String) {
        let substringRange = (self.string as NSString).range(of: forSubstring)
        self.addAttribute(NSAttributedString.Key.font, value: font, range: substringRange)
    }
    
    // MARK: - Color functions.
    func gmAttributedStringSet(mainColor: UIColor) {
        let allWordRange = (self.string as NSString).range(of: self.string)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: mainColor, range: allWordRange)
    }
    
    func gmAttributedStringSet(color: UIColor, forSubstring: String) {
        let substringRange = (self.string as NSString).range(of: forSubstring)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: substringRange)
    }
    
    // MARK: - Accesory functions.
    func gmAttributedStringSetStrikeThrough(size: CGFloat) {
        let allWordRange = (self.string as NSString).range(of: self.string)
        self.addAttribute(NSAttributedString.Key.strikethroughStyle, value: size, range: allWordRange)
    }
    
    func gmAttributedStringSetStrikeThrough(size: CGFloat, forSubstring: String) {
        let substringRange = (self.string as NSString).range(of: forSubstring)
        self.addAttribute(NSAttributedString.Key.strikethroughStyle, value: size, range: substringRange)
    }
}
