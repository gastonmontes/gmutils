//
//  GMString+Localizable.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation

extension String {
    func gmStringLocalizable() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
