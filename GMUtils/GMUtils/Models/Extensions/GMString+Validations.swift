//
//  GMString+Validations.swift
//  GMUtils
//
//  Created by Gaston Montes on 17/04/2020.
//  Copyright © 2020 Gaston  Montes. All rights reserved.
//

import Foundation

extension String {
    func gmIsEmpty() -> Bool {
        if self == "" || self.count == 0 {
            return true
        }
        
        return false
    }
    
    func gmIsValidEmail() -> Bool {
        guard !self.gmIsEmpty() else {
            return false
        }
        
        let stricterFilterString = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
        
        return emailTest.evaluate(with: self.trimmingCharacters(in: CharacterSet(charactersIn: " ")))
    }
    
    func gmIsValidPassword() -> Bool {
        guard !self.gmIsEmpty() else {
            return false
        }
        
        // Minimum 8 characters, at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character.
        let length = self.count >= 8
        let uppercasedLetter = self.gmStringHasAtLeastOneUppercasedLetter()
        let lowercasedLetter = self.gmStringHasAtLeastOneLowercasedLetter()
        let number = self.gmStringHasAtLeastOneNumber()
        let specialCharacter = self.gmStringHasAtLeastOneSpecialCharacter()
        
        return length && uppercasedLetter && lowercasedLetter && number && specialCharacter
    }
    
    func gmStringHasAtLeastOneUppercasedLetter() -> Bool {
        let uppercaseFilter = "[A-Z]"
        return self.range(of: uppercaseFilter, options: .regularExpression) != nil
    }
    
    func gmStringHasAtLeastOneLowercasedLetter() -> Bool {
        let lowercaseFilter = "[a-z]"
        return self.range(of: lowercaseFilter, options: .regularExpression) != nil
    }
    
    func gmStringHasAtLeastOneNumber() -> Bool {
        let numberFilter = "[0-9]"
        return self.range(of: numberFilter, options: .regularExpression) != nil
    }
    
    func gmStringHasAtLeastOneSpecialCharacter() -> Bool {
        let specialFilter = "[@#%*()]"
        return self.range(of: specialFilter, options: .regularExpression) != nil
    }
}
