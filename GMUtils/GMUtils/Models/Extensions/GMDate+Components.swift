//
//  GMDate+Components.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation

extension Date {
    // MARK: - Get components functions.
    func gmDateGetNanoseconds() -> Int {
        let calendar = Calendar.current
        
        return calendar.component(.nanosecond, from: self)
    }
    
    func dateGetMilliseconds() -> Int {
        let calendar = Calendar.current
        let nanoseconds = calendar.component(.nanosecond, from: self)
        return nanoseconds / 1000000
    }
    
    func gmDateGetComponentSeconds() -> Int {
        let calendar = Calendar.current
        
        return calendar.component(.second, from: self)
    }
    
    func gmDateGetComponentMinute() -> Int {
        let calendar = Calendar.current
        
        return calendar.component(.minute, from: self)
    }
    
    func gmDateGetComponentHour() -> Int {
        let calendar = Calendar.current
        
        return calendar.component(.hour, from: self)
    }
    
    func gmDateGetComponentDay() -> Int {
        let calendar = Calendar.current
        
        return calendar.component(.day, from: self)
    }
    
    func gmDateGetComponentMonth() -> Int {
        let calendar = Calendar.current
        
        return calendar.component(.month, from: self)
    }
    
    func gmDateGetComponentYear() -> Int {
        let calendar = Calendar.current
        
        return calendar.component(.year, from: self)
    }
    
    // MARK: - Creation functions.
    static func gmDateWithComponents(nanoseconds: Int = 0,
                                     seconds: Int = 0,
                                     minutes: Int = 0,
                                     hours: Int = 0,
                                     day: Int = 1,
                                     month: Int = 1,
                                     year: Int = 1970,
                                     timeZone: TimeZone = NSTimeZone.system) -> Date? {
        let calendar = Calendar.current
        
        var components = DateComponents()
        components.nanosecond = nanoseconds
        components.second = seconds
        components.minute = minutes
        components.hour = hours
        components.day = day
        components.month = month
        components.year = year
        components.timeZone = timeZone
        
        return calendar.date(from: components as DateComponents)
    }
    
    // MARK: - Update functions.
    func gmDateUpdateComponents(nanoseconds: Int? = nil,
                                seconds: Int? = nil,
                                minutes: Int? = nil,
                                hours: Int? = nil,
                                day: Int? = nil,
                                month: Int? = nil,
                                year: Int? = nil,
                                timeZone: TimeZone? = nil) -> Date? {
        let currentCalendar = Calendar.current

        var dateComponents = currentCalendar.dateComponents([.day, .month, .year], from: self)
        dateComponents.nanosecond = nanoseconds ?? dateComponents.nanosecond
        dateComponents.second = seconds ?? dateComponents.second
        dateComponents.minute = minutes ?? dateComponents.minute
        dateComponents.hour = hours ?? dateComponents.hour
        dateComponents.day = day ?? dateComponents.day
        dateComponents.month = month ?? dateComponents.month
        dateComponents.year = year ?? dateComponents.year
        dateComponents.timeZone = timeZone

        return currentCalendar.date(from: dateComponents)
    }
}
