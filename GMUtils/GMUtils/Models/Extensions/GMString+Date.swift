//
//  GMString+Date.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation

extension String {
    func gmStringDate(format: String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        
        return formatter.date(from: self)
    }
}
