//
//  GMBaseService.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation

// MARK: - Constants, structs & Enums.
private let kConfigurationDefaultRetry = Int(0)
private let kConfigurationDefaultTimeout = Double(20)

private let kErrorBaseServiceDomain = "GMBaseServiceDomain"
private let kErrorNoInternetDescription = "GMBaseService.Error.NoInternet.Description"
private let kErrorUnknownDescription = "GMBaseService.Error.Unknown.Description"
private let kErrorEncodingDescription = "GMBaseService.Error.Encoding.Description"
private let kErrorDictionaryCodeKey = "code"
private let kErrorDictionaryTitleKey = "title"
private let kErrorDictionaryMessageListKey = "message"

// MARK: - Protocols.
protocol GMBaseServiceRequestProtocol {
    func gmServiceRequestURLBaseString() -> String
    func gmServiceRequestURLPathString() -> String?
    func gmServiceRequestParameters() -> Dictionary<String, Any>?
    func gmServiceRequestHeader() -> Dictionary<String, String>?
    func gmServiceRequestRetryCount() -> Int
    func gmServiceRequestTimeout() -> Double
    func gmServiceParameterEncoding() -> GMServiceEncodingType
}

// MARK: - GMBaseServiceRequestProtocol implementation.
extension GMBaseServiceRequestProtocol {
    func gmServiceRequestURLBaseString() -> String {
        return "www.base-url.com"
    }
    
    func gmServiceRequestParameters() -> Dictionary<String, Any>? {
        return nil
    }
    
    func gmServiceRequestHeader() -> Dictionary<String, String>? {
        return nil
    }
    
    func gmServiceRequestRetryCount() -> Int {
        return kConfigurationDefaultRetry
    }
    
    func gmServiceRequestTimeout() -> Double {
        return kConfigurationDefaultTimeout
    }
    
    func gmServiceParameterEncoding() -> GMServiceEncodingType {
        return .url
    }
}

// MARK: - Handlers blocks.
typealias GMServiceSuccessHandler = ([String : Any]) -> Void
typealias GMServiceFailHandler = (GMBaseServiceError) -> Void

enum GMBaseServiceMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

enum GMServiceEncodingType {
    case url
    case json
}

class GMBaseService: NSObject {
    // MARK: - Vars.
    private var serviceCurrentRequest: URLRequest?
    private var serviceSession: URLSession?
    private var serviceCurrentDataTask: URLSessionDataTask?
    private var serviceRetryCount = Int(0)
    
    // MARK: - Service call.
    /// Call to services.
    ///
    /// - Parameters:
    ///   - serviceModel: The model to get path, parameters and header for request.
    ///   - method: The HTTP method (GET, PUT, POST or DELETE,).
    ///   - successHandler: Success handler.
    ///   - failHandler: Fail handler.
    private func serviceCall(serviceModel: GMBaseServiceRequestProtocol,
                             method: GMBaseServiceMethod,
                             successHandler: GMServiceSuccessHandler?,
                             failHandler: GMServiceFailHandler?) {
        // Check internet connection.
        guard GMReachability.gmIsReachable() else {
            let baseServiceError = GMBaseServiceError(domain: kErrorBaseServiceDomain,
                                                      description: NSLocalizedString(kErrorNoInternetDescription, comment: ""),
                                                      code: GMErrorDomainCodes.errorDomainCodeNoInternet)
            failHandler?(baseServiceError)
            return
        }
        
        // Create the url.
        guard let url = URL(string: self.serviceCreateRequestURLString(serviceModel: serviceModel)) else {
            let baseServiceError = GMBaseServiceError(domain: kErrorBaseServiceDomain,
                                                      description: NSLocalizedString(kErrorUnknownDescription, comment: ""),
                                                      code: GMErrorDomainCodes.errorDomainCodeUnknown)
            failHandler?(baseServiceError)
            return
        }
        
        // Cancel previous task.
        if let task = self.serviceCurrentDataTask {
            task.cancel()
        }
        
        // Create Session.
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = serviceModel.gmServiceRequestTimeout()
        configuration.timeoutIntervalForResource = serviceModel.gmServiceRequestTimeout()
        self.serviceSession = URLSession(configuration: configuration)
        
        // Create the Request.
        self.serviceCurrentRequest = URLRequest(url: url)
        self.serviceCurrentRequest!.httpMethod = method.rawValue
        self.serviceCurrentRequest!.allHTTPHeaderFields = self.serviceCreateRequestHeaders(serviceModel: serviceModel)
        
        do {
            try self.serviceSetBody(serviceModel: serviceModel)
        } catch {
            guard let baseServiceError = error as? GMBaseServiceError else {
                return
            }
            
            failHandler?(baseServiceError)
        }
        
        // Create the completion handler.
        let completionHandler = { (data: Data?, response: URLResponse?, error: Error?) in
            if let baseServiceError = error as? GMBaseServiceError {
                failHandler?(baseServiceError)
                return
            }
            
            if let data = data {
                do {
                    if let responseData = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] {
                        if let errorCode = responseData[kErrorDictionaryCodeKey] as? Int, errorCode > 299 {
                            let errorDescriptionList = responseData[kErrorDictionaryMessageListKey] as? Array<String>
                            let errorDescription = errorDescriptionList?.first
                            let errorTitle = responseData[kErrorDictionaryTitleKey] as? String
                            
                            failHandler?(GMBaseServiceError(domain: errorTitle ?? kErrorBaseServiceDomain,
                                                            description: errorDescription ?? "",
                                                            code: errorCode))
                            return
                        }
                        
                        successHandler?(responseData)
                    }
                } catch {
                    guard let baseServiceError = error as? GMBaseServiceError else {
                        return
                    }
                    
                    failHandler?(baseServiceError)
                }
            } else if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode > 299 {
                let errorDescription = HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
                let baseServiceError = GMBaseServiceError(domain: kErrorBaseServiceDomain,
                                                          description: errorDescription,
                                                          code: httpResponse.statusCode)
                
                failHandler?(baseServiceError)
                return
            }
        }
        
        // Create the Data task.
        self.serviceCurrentDataTask = self.serviceSession!.dataTask(with: self.serviceCurrentRequest!, completionHandler: completionHandler)
        self.serviceCurrentDataTask?.resume()
    }
    
    // MARK: - Service components functions.
    /// Create the request paths.
    /// - Parameter serviceModel: The model to get the path for request.
    ///
    /// - Returns: The path to requets.
    private func serviceCreateRequestURLString(serviceModel: GMBaseServiceRequestProtocol) -> String {
        let baseURL = serviceModel.gmServiceRequestURLBaseString()
        let pathURL = serviceModel.gmServiceRequestURLPathString()
        return (pathURL != nil) ? baseURL + pathURL! : baseURL
    }
    
    /// Create the request headers.
    /// - Parameter serviceModel: The model to get the headers.
    ///
    /// - Returns: Headers to make the request.
    private func serviceCreateRequestHeaders(serviceModel: GMBaseServiceRequestProtocol) -> [String : String]? {
        // Add custom headers here.
        let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let buildNumber = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
        
        var httpHeaders = [ "iOS" : "\(versionNumber) (\(buildNumber))"]
        
        if let modelHeaders = serviceModel.gmServiceRequestHeader() {
            for (key, value) in modelHeaders {
                httpHeaders[key] = value
            }
        }
        
        return httpHeaders
    }
    
    /// Add body to current request.
    /// - Parameter serviceModel: The model to get the encoding and body parameters.
    /// Create the request headers.
    ///
    /// - Returns: Headers to make the request.
    private func serviceSetBody(serviceModel: GMBaseServiceRequestProtocol) throws {
        guard self.serviceCurrentRequest != nil else {
            return
        }
        
        switch serviceModel.gmServiceParameterEncoding() {
        case .url:
            self.serviceCurrentRequest!.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")

            if let parameters = serviceModel.gmServiceRequestParameters() {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)

                    if let jsonString = String(data: jsonData, encoding: .utf8) {
                        self.serviceCurrentRequest?.httpBody = "\(jsonString.gmStringCreateURLQueryPercentScape())".data(using: .utf8, allowLossyConversion: false)
                    } else {
                        throw GMBaseServiceError(domain: kErrorBaseServiceDomain,
                                                 description: kErrorEncodingDescription.gmStringLocalizable(),
                                                 code: GMErrorDomainCodes.errorDomainCodeEncoding)
                    }
                } catch {
                    throw GMBaseServiceError(domain: error.gmErrorDomain(),
                                             description: error.localizedDescription,
                                             code: error.gmErrorCode())
                }
            }
        case .json:
            self.serviceCurrentRequest!.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            if let parameters = serviceModel.gmServiceRequestParameters() {
                do {
                    self.serviceCurrentRequest!.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                } catch {
                    throw GMBaseServiceError(domain: kErrorBaseServiceDomain,
                                             description: kErrorEncodingDescription.gmStringLocalizable(),
                                             code: GMErrorDomainCodes.errorDomainCodeEncoding)
                }
            }
        }
    }
    
    // MARK: - Session functions.
    private func configureSession(serviceModel: GMBaseServiceRequestProtocol) {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = serviceModel.gmServiceRequestTimeout()
        configuration.timeoutIntervalForResource = serviceModel.gmServiceRequestTimeout()
        self.serviceSession = URLSession(configuration: configuration)
    }
    
    // MARK: - HTTP Verbs operations.
    /// Queries our database (Unity) and retrieves content.
    ///
    /// - Parameters:
    ///   - serviceModel: The model to get path, parameters and header for request.
    ///   - successHandler: Success handler.
    ///   - failHandler: Fail handler.
    final func gmServiceGet(serviceModel: GMBaseServiceRequestProtocol, successHandler: GMServiceSuccessHandler?, failHandler: GMServiceFailHandler?) {
        self.serviceRetryCount = 0
        self.gmServiceCallWithRetry(serviceModel: serviceModel, method: .get, successHandler: successHandler, failHandler: failHandler)
    }
    
    /// Sends data to the server and then saves it.
    ///
    /// - Parameters:
    ///   - serviceModel: The model to get path, parameters and header for request.
    ///   - successHandler: Success handler.
    ///   - failHandler: Fail handler.
    final func gmServicePost(serviceModel: GMBaseServiceRequestProtocol, successHandler: GMServiceSuccessHandler?, failHandler: GMServiceFailHandler?) {
        self.serviceRetryCount = 0
        self.gmServiceCallWithRetry(serviceModel: serviceModel, method: .post, successHandler: successHandler, failHandler: failHandler)
    }
    
    /// Post data to the server if not exists or update data if already exists.
    ///
    /// - Parameters:
    ///   - serviceModel: The model to get path, parameters and header for request.
    ///   - successHandler: Success handler.
    ///   - failHandler: Fail handler.
    final func gmServicePut(serviceModel: GMBaseServiceRequestProtocol, successHandler: GMServiceSuccessHandler?, failHandler: GMServiceFailHandler?) {
        self.serviceRetryCount = 0
        self.gmServiceCallWithRetry(serviceModel: serviceModel, method: .put, successHandler: successHandler, failHandler: failHandler)
    }
    
    /// Delete data from the server.
    ///
    /// - Parameters:
    ///   - serviceModel: The model to get path, parameters and header for request.
    ///   - successHandler: Success handler.
    ///   - failHandler: Fail handler.
    final func gmServiceDelete(serviceModel: GMBaseServiceRequestProtocol, successHandler: GMServiceSuccessHandler?, failHandler: GMServiceFailHandler?) {
        self.serviceRetryCount = 0
        self.gmServiceCallWithRetry(serviceModel: serviceModel, method: .delete, successHandler: successHandler, failHandler: failHandler)
    }
    
    // MARK: - Cancel, pause, retry, resume functions.
    func gmServiceCancelCurrentRequest() {
        self.serviceCurrentDataTask?.cancel()
    }
    
    func gmServicePauseRequest() {
        self.serviceCurrentDataTask?.suspend()
    }
    
    func gmServiceResumeRequest() {
        self.serviceCurrentDataTask?.resume()
    }
    
    // MARK: - Retry functions.
    private func gmServiceCallWithRetry(serviceModel: GMBaseServiceRequestProtocol, method: GMBaseServiceMethod, successHandler: GMServiceSuccessHandler?, failHandler: GMServiceFailHandler?) {
        self.serviceCall(serviceModel: serviceModel, method: method, successHandler: successHandler) { error in
            if self.serviceRetryCount < serviceModel.gmServiceRequestRetryCount() {
                self.serviceRetryCount += 1
                self.gmServiceCallWithRetry(serviceModel: serviceModel, method: method, successHandler: successHandler, failHandler: failHandler)
            } else {
                failHandler?(error)
            }
        }
    }
}
