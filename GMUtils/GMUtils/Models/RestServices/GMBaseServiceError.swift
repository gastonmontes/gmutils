//
//  GMBaseServiceError.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation

// MARK: - Constants, structs & Enums.
private let kErrorTitleDefault = "GMBaseServiceError"

class GMErrorDomainCodes : NSObject {
    static let errorDomainCodeNoInternet: Int = -1009
    static let errorDomainCodeUserCancelled: Int = -102
    static let errorDomainCodeUnknown: Int = -1745
    static let errorDomainCodeEncoding: Int = -3246
}

// MARK: - Protocols.
protocol GMBaseServiceErrorProtocol: Error {
    var gmErrorDomain: String { get }
    var gmErrorDescription: String { get }
    var gmErrorCode: Int { get }
}

// MARK: - Extensions.
extension Error {
    func gmErrorDomain() -> String {
        return (self as NSError).domain
    }
    
    func gmErrorCode() -> Int {
        return (self as NSError).code
    }
    
    func gmEIsErrorCodeNoInternet() -> Bool {
        return GMErrorDomainCodes.errorDomainCodeNoInternet == self.gmErrorCode()
    }
}

class GMBaseServiceError: NSObject, GMBaseServiceErrorProtocol {
    private(set) var gmErrorDomain: String
    private(set) var gmErrorDescription: String
    private(set) var gmErrorCode: Int
    
    init(domain: String?, description: String, code: Int) {
        self.gmErrorDomain = domain ?? kErrorTitleDefault
        self.gmErrorDescription = description
        self.gmErrorCode = code
    }
}
