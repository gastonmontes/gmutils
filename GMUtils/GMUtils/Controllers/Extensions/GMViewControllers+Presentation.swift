//
//  GMViewControllers+Presentation.swift
//  GMUtils
//
//  Created by Gaston  Montes on 11/07/2019.
//  Copyright © 2019 Gaston  Montes. All rights reserved.
//

import Foundation
import MessageUI
import UIKit

extension UIViewController {
    func gmPresentAlert(title: String = "", message: String = "", actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        
        for action in actions {
            alertController.addAction(action)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func gmPresentMail(mailDelegate: MFMailComposeViewControllerDelegate) {
        guard MFMailComposeViewController.canSendMail() else {
            return
        }
        
        let picker = MFMailComposeViewController()
        picker.mailComposeDelegate = mailDelegate
        self.present(picker, animated: true, completion: nil)
    }
    
    func gmPresentActionSheet(title: String = "", message: String = "", actions: [UIAlertAction]) {
        let actionSheet = UIAlertController(title: title,
                                            message: message,
                                            preferredStyle: .actionSheet)
        
        for action in actions {
            actionSheet.addAction(action)
        }
        
        self.navigationController?.present(actionSheet, animated: true, completion: nil)
    }
}
