//
//  GMViewController+Segue.swift
//  GMUtils
//
//  Created by Gaston Montes on 17/04/2020.
//  Copyright © 2020 Gaston  Montes. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func gmControllerCanPerformSegue(withIdentifier identifier: String) -> Bool {
        guard let segues = self.value(forKey: "storyboardSegueTemplates") as? [NSObject] else {
            return false
        }
        
        for segue in segues {
            if let segueID = segue.value(forKey: "identifier") as? String, segueID == identifier {
                return true
            }
        }
        
        return false
    }
}
